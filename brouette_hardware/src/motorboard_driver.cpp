#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include "termios.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "sys/types.h"
#include "math.h"
#include "ros/ros.h"
#include "sstream"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Int64.h"
#include "time.h"
#include "brouette_hardware/motorboard_driver.h"
#include <list>


/**
 * @brief Constructor of the Class
 */
MotorBoard::MotorBoard(const unsigned char address){
  address_ = address;
  encoder_register_[0] = ENCODER_A_REGISTER; // encoder 1 
  encoder_register_[1] = ENCODER_B_REGISTER; // encoder 2 
  motor_register_[0] = MOTOR_A_REGISTER;   // motor 1 
  motor_register_[1] = MOTOR_B_REGISTER;   // motor 2 
}

/**
 * @brief Destroy the motor drivers
 */
MotorBoard::~MotorBoard(){
  //DESTROYPTR();
  ROS_WARN("MotorBoard/Destructor - object killed");
}

void MotorBoard::changeI2cBusAddress(int file_pointer){
	buffer_[0] = 0x55; buffer_[1] = 0xB0; buffer_[2] = 0x10;	buffer_[3] = 0x01; buffer_[4] = 0xA0;

	if (write(file_pointer, buffer_, 5) < 0) perror("change_I2C_bus_address write");	// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("change_I2C_bus_address tcdrain");
	if (read(file_pointer, buffer_, 1) < 0) perror("change_I2C_bus_address read");	// Read data back from USB-ISS, module ID and software version

	buffer_[0] = 0x55; buffer_[1] = 0xB0; buffer_[2] = 0x10;	buffer_[3] = 0x01; buffer_[4] = 0xAA;

	if (write(file_pointer, buffer_, 5) < 0) perror("change_I2C_bus_address write");	// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("change_I2C_bus_address tcdrain");
	if (read(file_pointer, buffer_, 1) < 0) perror("change_I2C_bus_address read");	// Read data back from USB-ISS, module ID and software version

	buffer_[0] = 0x55; buffer_[1] = 0xB0;	buffer_[2] = 0x10;	buffer_[3] = 0x01; buffer_[4] = 0xA5;

	if (write(file_pointer, buffer_, 5) < 0) perror("change_I2C_bus_address write");	// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("change_I2C_bus_address tcdrain");
	if (read(file_pointer, buffer_, 1) < 0) perror("change_I2C_bus_address read");	// Read data back from USB-ISS, module ID and software version

	buffer_[0] = 0x55; buffer_[1] = 0xB0;	buffer_[2] = 0x10;	buffer_[3] = 0x01; buffer_[4] = 0xB2;

	if (write(file_pointer, buffer_, 5) < 0) perror("change_I2C_bus_address write");	// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("change_I2C_bus_address tcdrain");
	if (read(file_pointer, buffer_, 1) < 0) perror("change_I2C_bus_address read");	// Read data back from USB-ISS, module ID and software version

   	ROS_INFO("changed bus address");
}

void MotorBoard::setI2cMode(int file_pointer)
{
	buffer_[0] = 0x5A;	buffer_[1] = 0x02;	buffer_[2] = 0x40;	buffer_[3] = 0x00;
	// USB_ISS command; Set mode; Set mode to 100KHz I2C; Spare pins set to output low

	if (write(file_pointer, buffer_, 4) < 0) perror("set_i2c_mode write");	// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("set_i2c_mode tcdrain");
	if (read(file_pointer, buffer_, 2) < 0) perror("set_i2c_mode read");		// Read back error byte
	if(buffer_[0] != 0xFF)						// If first returned byte is not 0xFF then an error has occured
	{
		ROS_ERROR("**set_i2c_mode: Error setting I2C mode!**");
		error_ = 0xFF;						// Set error byte
	}

    ROS_INFO("Set Mode to I2C");
}

void MotorBoard::displayVersion(int file_pointer)
{
	buffer_[0] = 0x5A; buffer_[1] = 0x01;
	// USB_ISS byte; Software return byte

	if (write(file_pointer, buffer_, 2) < 0) perror("display_version write");	// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("display_version tcdrain");
	if (read(file_pointer, buffer_, 3) < 0) perror("display_version read");	// Read data back from USB-ISS, module ID and software version

	ROS_INFO("MD25 Module ID: %u", buffer_[0]);
	ROS_INFO("MD25 Software v: %u", buffer_[1]);
    ROS_INFO("MD25 Current Mode: %x", buffer_[2]);
}


void MotorBoard::disableTimeout(int file_pointer, unsigned char driver_address)
{
    buffer_[0] = 0x55; buffer_[1] = driver_address; buffer_[2] = 0x10; buffer_[3] = 0x01; buffer_[4] = 0x32;
	// Primary USB-ISS command; Address of MD25; Command register - Command; Number of data bytes to write; Data byte (reset encoders)

	if (write(file_pointer, buffer_, 5) < 0) perror("disable_timeout write");		// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("disable_timeout tcdrain");
	if (read(file_pointer, buffer_, 1) < 0) perror("disable_timeout read");		// Read back error byte	

	ROS_INFO("Disabled timeout from %x", driver_address);
  }

void MotorBoard::resetEncoder(int file_pointer, unsigned char driver_address)
{
	buffer_[0] = 0x55;							// Primary USB-ISS command
	buffer_[1] = driver_address;				// Address of MD25
	buffer_[2] = 0x10;							// Command register   // Command
	buffer_[3] = 0x01;							// Number of data bytes to write
	buffer_[4] = 0x20;							// Data byte (reset encoders)

	if (write(file_pointer, buffer_, 5) < 0) perror("reset encoder write");	// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("reset encoder tcdrain");
	if (read(file_pointer, buffer_, 1) < 0) perror("reset encoder read");		// Read back error byte	

    ROS_INFO("Encoder from %x registers are reset to zero", driver_address);
}

void MotorBoard::disableSpeedReg(int file_pointer, unsigned char driver_address)
{                        
	buffer_[0] = 0x55; buffer_[1] = driver_address; buffer_[2] = 0x10; buffer_[3] = 0x01; buffer_[4] = 0x30;
	// Primary USB-ISS command; Address of MD25; Command register - Command; Number of data bytes to write; Data byte (reset encoders)

	if (write(file_pointer, buffer_, 5) < 0) perror("disable_speed_reg write");		// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("disable_speed_reg tcdrain");
	if (read(file_pointer, buffer_, 1) < 0) perror("disable_speed_reg read");		// Read back error byte	

	ROS_INFO("Disabled speed regulation from %x", driver_address);
}

void MotorBoard::setMotorSpeed(int file_pointer, unsigned char driver_address, int motor_register_number, unsigned char speed)
{
	buffer_[0] = 0x55; buffer_[1] = driver_address; buffer_[2] = motor_register_[motor_register_number]; buffer_[3] = 0x01;	buffer_[4] = speed;
	// Primary USB-ISS command; Address of MD25; Command register  - Motor Speed;  Number of data bytes to write; Data byte 

	if (write(file_pointer, buffer_, 5) < 0) perror("set_motor_speed write");		// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("set_motor_speed tcdrain");
	if (read(file_pointer, buffer_, 1) < 0) perror("set_motor_speed read");		// Read back error byte	
}

unsigned char MotorBoard::getMotorRegistrer(int i){
  return motor_register_[i];
}

unsigned char MotorBoard::getEncoderRegistrer(int i){
  return encoder_register_[i];
}

unsigned char MotorBoard::getError(){
  return error_;
}

int MotorBoard::readEncoder(int file_pointer, unsigned char driverAddress, int encoder_register_number)
{
  buffer_[0] = 0x55; buffer_[1] = driverAddress + 1; buffer_[2] = encoder_register_[encoder_register_number]; buffer_[3] = 0x04;
  // Primary USB-ISS command; Address of MD25 with R/W bit high; Internal register of MD25 to read (encoder); Number of bytes we wish to read
  if (write(file_pointer, buffer_, 4) < 0) perror("read_encoder write");	// Write data to USB-ISS
  if (tcdrain(file_pointer) < 0) perror("read_encoder tcdrain");
  if (read(file_pointer, buffer_, 4) < 0) perror("read_encoder read");	// Read back error byte

  int encoder_value = buffer_[0];
  for (int i = 1; i<4; i++) {
	encoder_value <<= 8;
    encoder_value += buffer_[i];
  }

  return encoder_value;
}

void MotorBoard::readBatteryVoltage(int file_pointer, unsigned char driver_address)
{
	buffer_[0] = 0x55;						// Primary USB-ISS command
	buffer_[1] = driver_address + 1;			// Address of MD25
	buffer_[2] = 0x0A;
	buffer_[3] = 0x01;							// Number of data bytes to write

	if (write(file_pointer, buffer_, 4) < 0) perror("read_battery_voltage write");		// Write data to USB-ISS
	if (tcdrain(file_pointer) < 0) perror("read_battery_voltage tcdrain");
	if (read(file_pointer, buffer_, 1) < 0) perror("read_battery_voltage read");		// Read back error byte	
	battery_voltage_ = buffer_[0];
}

bool MotorBoard::getDebugMode(void) {
	return debug_;
}

double MotorBoard::getBatteryVoltage(void){
	return battery_voltage_/10.0;
}

unsigned char MotorBoard::getAddress(){
  return address_;
}
