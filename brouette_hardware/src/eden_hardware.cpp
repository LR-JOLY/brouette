#include "brouette_hardware/eden_hardware.h"
#include <boost/assign/list_of.hpp>
#include "controller_manager/controller_manager.h"
#include "hardware_interface/actuator_state_interface.h"
#include "math.h"



Eden::Eden(ros::NodeHandle* nodehandle):nh_(*nodehandle)
{
  // communication opening 
  const char *device = "/dev/usb2iss";
  // add motor
  // TODO : take data in a config file
  double PID_leg_motor[3] = {1.0, 0.0, 0.0};
  double PID_wheel_motor[3] = {1.0, 0.0, 0.0};
  int leg_encoder_incr_number = 121;
  int wheel_encoder_incr_number = 121;
  double leg_transmission_ratio = 810.0;
  double wheel_transmission_ratio = 17.0*40.0/100.0;
  usb2iss_.addMotor("fr_leg_joint","JointPositionController",FRONT_RIGHT_MOTORBOARD, leg_encoder_incr_number, leg_transmission_ratio, PID_leg_motor);
  usb2iss_.addMotor("fr_wheel_joint","JointVelocityController",FRONT_RIGHT_MOTORBOARD, wheel_encoder_incr_number, wheel_transmission_ratio, PID_wheel_motor);
  usb2iss_.addMotor("fl_leg_joint","JointPositionController",FRONT_LEFT_MOTORBOARD, leg_encoder_incr_number, leg_transmission_ratio, PID_leg_motor);
  usb2iss_.addMotor("fl_wheel_joint","JointVelocityController",FRONT_LEFT_MOTORBOARD, wheel_encoder_incr_number, wheel_transmission_ratio, PID_wheel_motor);
  usb2iss_.addMotor("rr_leg_joint","JointPositionController",REAR_RIGHT_MOTORBOARD, leg_encoder_incr_number, leg_transmission_ratio, PID_leg_motor);
  usb2iss_.addMotor("rr_wheel_joint","JointVelocityController",REAR_RIGHT_MOTORBOARD, wheel_encoder_incr_number, wheel_transmission_ratio, PID_wheel_motor);
  usb2iss_.addMotor("rl_leg_joint","JointPositionController",REAR_LEFT_MOTORBOARD, leg_encoder_incr_number, leg_transmission_ratio, PID_leg_motor);
  usb2iss_.addMotor("rl_wheel_joint","JointVelocityController",REAR_LEFT_MOTORBOARD, wheel_encoder_incr_number, wheel_transmission_ratio, PID_wheel_motor);
  usb2iss_.openCommunicationDevice(device);
  usb2iss_.wakeUpBoards();

  ros::V_string joint_names = usb2iss_.getJointNameVector();
  
  for (unsigned int i = 0; i < joint_names.size(); i++)
  {
    hardware_interface::JointStateHandle joint_state_handle(joint_names[i],
                                                            &joints_[i].position, &joints_[i].velocity,
                                                            &joints_[i].effort);
    joint_state_interface_.registerHandle(joint_state_handle);
    hardware_interface::JointHandle joint_handle(joint_state_handle, &joints_[i].command);

    if ( i % 2 == 0)
    {
        position_joint_interface_.registerHandle(joint_handle);
    }
    else
    {
        velocity_joint_interface_.registerHandle(joint_handle);
    }

  }
  registerInterface(&joint_state_interface_);
  registerInterface(&velocity_joint_interface_);
  registerInterface(&position_joint_interface_);
}

Eden::~Eden(){
  //DESTROYPTR();
  ROS_WARN("Eden/Destructor - object killed");
}


void Eden::getTarget(void)
{
    double target[MOTORS_NUMBER];
    for (int i = 0; i<MOTORS_NUMBER; i++) {
        target[i] = joints_[i].command;
    }
    usb2iss_.setTarget(target);
}

void Eden::updateHardware()
{
  double *position_measured[MOTORS_NUMBER];
  double *velocity_measured[MOTORS_NUMBER];
  // First read the encoder data
  usb2iss_.readEncoderData(position_measured, velocity_measured);
  // then put those data in the joints_ structure
  for (int i = 0; i<MOTORS_NUMBER; i++) {
    joints_[i].position = *position_measured[i]; // WARNING : encoder roll over not covered !!!
    joints_[i].velocity = *velocity_measured[i];
  }
  // Send the new command to the motor
  usb2iss_.updateMotorSpeed();
}

void Eden::restorePort()
{
  usb2iss_.closeCommunicationDevice();
}