#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include "termios.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "sys/types.h"
#include "math.h"
#include "ros/ros.h"
#include "sstream"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Int64.h"
#include "time.h"
#include "brouette_hardware/motorboard_driver.h"
#include "brouette_hardware/motorboard_superviser.h"
#include <list>


/**
 * @brief Constructor of the Class
 */
MotorboardSuperviser::MotorboardSuperviser()
{
//  nh->getParam("P", P);
//  nh->getParam("I", I);
//  nh->getParam("D", D);
//  nh->getParam("debug", debug);
  // init publishers
//  ros_battery_voltage_pub = nh->advertise<std_msgs::Float64>("battery_voltage", 10);
//  if (debug) {
//	  ros_wheel_speed_pub = nh->advertise<std_msgs::Float64MultiArray>("wheel_speed_debug", 10);

    initialiseList(128, velocity_command_);
    initialiseList(0.0, velocity_measurement_);
}

MotorboardSuperviser::~MotorboardSuperviser()
{
    closeCommunicationDevice();
}

void MotorboardSuperviser::wakeUpBoards(void)
{
    // Get firmware version of the communication device
    for (MotorBoard motorboard : motorboard_list)
    {
        // Display firmware info
        motorboard.displayVersion(communication_device_pointer_);
        // set the MD25 to I2C mode (and not serial)
        motorboard.setI2cMode(communication_device_pointer_);
        if(!motorboard.getError())
        {
            motorboard.disableTimeout(communication_device_pointer_, motorboard.getAddress());             // Disable Timeout
            motorboard.resetEncoder(communication_device_pointer_, motorboard.getAddress());               // Reset encoder register
            motorboard.disableSpeedReg(communication_device_pointer_, motorboard.getAddress());            // Disable speed regulation
        }
        else
        {
            ROS_ERROR("Motorboard (address %c) can not be reached",motorboard.getAddress());
            ROS_ERROR("Eden won't be abble to work properly");
        }
    }
}


int MotorboardSuperviser::setMotorboardList(unsigned char motorboard_address){
    int i = -1;
    unsigned char address;
    for (MotorBoard motorboard : motorboard_list)
    {
        address = motorboard.getAddress();
        if ( address == motorboard_address)
        {
            break;
        }
        ++i;
    }
    if (address != motorboard_address)
    {
        MotorBoard motorboardTmp(motorboard_address);
        motorboard_list.push_back(motorboardTmp);
    }
    return i;
}

void MotorboardSuperviser::addMotor(std::string joint_name, std::string controller_type, unsigned char motorboard_address, int encoder_increment_number, double transmission_ratio, double PID[3]){
    Motor motor(joint_name, controller_type, encoder_increment_number, transmission_ratio, PID[3]);
    motor.setBoardIndex(this->setMotorboardList(motorboard_address));
    motor_list.push_back(motor);
}


void MotorboardSuperviser::openCommunicationDevice(const char *device){
  // set driver port
	communication_device_pointer_ = open(device, O_RDWR | O_NOCTTY);
}

int MotorboardSuperviser::getCommunicationDevicePointer(void){
  // get driver port
	return communication_device_pointer_;
}

void MotorboardSuperviser::closeCommunicationDevice(void){
  // close driver port
	close(communication_device_pointer_);
}

void MotorboardSuperviser::initialiseList(int value, int* list){
  for (int i; i< MOTORS_NUMBER; i++) {
      list[i] = value;
  }
}

void MotorboardSuperviser::initialiseList(double value, double* list){
  for (int i; i< MOTORS_NUMBER; i++) {
      list[i] = value;
  }
}

ros::V_string MotorboardSuperviser::getJointNameVector()
{
    ros::V_string joint_name_vector;
    for (Motor motor : motor_list){
        joint_name_vector.push_back(motor.getJointName());
    }
    return joint_name_vector;
}

void MotorboardSuperviser::readEncoderData(double *pos[MOTORS_NUMBER], double *vel[MOTORS_NUMBER])
{
    for (Motor motor : motor_list)
    {
        for (int i; i<2; ++i)
        {
            std::list<MotorBoard>::iterator it = motorboard_list.begin();
            std::advance(it,motor.getBoardIndex()); 
            MotorBoard motorboard = *it;
            motor.populatePositionMeasurement( motorboard.readEncoder(communication_device_pointer_,  motorboard.getAddress(), i), pos[i], vel[i]);
        }
    } 
}

void MotorboardSuperviser::setTarget(double target[MOTORS_NUMBER])
{
    int i = 0;
    for (Motor motor : motor_list)
    {
        motor.setTarget(target[i]);
    }
}

void MotorboardSuperviser::updateMotorSpeed(void)
{
    for (Motor motor : motor_list)
    {
        for (int i; i<2; ++i)
        {
            std::list<MotorBoard>::iterator it = motorboard_list.begin();
            std::advance(it,motor.getBoardIndex()); 
            MotorBoard motorboard = *it;
            motorboard.setMotorSpeed(communication_device_pointer_,  motorboard.getAddress(), i, motor.getCommand());
        }
    } 
}

/*
void MotorBoard::publish_target_and_measure(double *t[4]) {
	if (debug) {
		std_msgs::Float64MultiArray msg;
		for (int i=0; i<4; i++){
            msg.data.push_back(*t[i]);
            msg.data.push_back(velocity_mes[i]);
		}
	ros_wheel_speed_pub.publish(msg);	
	}
}
*/

/*
void MotorBoard::publish_battery_voltage(){
	std_msgs::Float64 battery_voltage_msg;
	battery_voltage_msg.data = battery_voltage/10.0;
	ros_battery_voltage_pub.publish(battery_voltage_msg);
}
*/
/*
void MotorBoard::displayVersion(int filePointer)
{
	m_buffer[0] = 0x5A; m_buffer[1] = 0x01;
	// USB_ISS byte; Software return byte

	if (write(filePointer, m_buffer, 2) < 0) perror("display_version write");	// Write data to USB-ISS
	if (tcdrain(filePointer) < 0) perror("display_version tcdrain");
	if (read(filePointer, m_buffer, 3) < 0) perror("display_version read");	// Read data back from USB-ISS, module ID and software version

	printf("USB-ISS Module ID: %u", m_buffer[0]);
	printf("USB-ISS Software v: %u", m_buffer[1]);
    printf("USB-ISS Current Mode: %x", m_buffer[2]);
}
*/