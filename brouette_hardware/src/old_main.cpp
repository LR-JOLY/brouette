#include <brouette_hardware/eden_hardware.h>
#include "controller_manager/controller_manager.h"
#include <ros/callback_queue.h>

#include <boost/chrono.hpp>

typedef boost::chrono::steady_clock time_source;

void controlLoop(Eden &my_eden,
                 controller_manager::ControllerManager &controller_manager,
                 time_source::time_point &last_time)
{

  double *target[MOTORS_NUMBER];
  double *command[MOTORS_NUMBER];

  // Calculate monotonic time difference
  time_source::time_point this_time = time_source::now();
  boost::chrono::duration<double> elapsed_duration = this_time - last_time;
  ros::Duration elapsed(elapsed_duration.count());
  last_time = this_time;

  // Process control loop
  // Get velocity target from holomonic controller
  my_eden.getTarget();
  // Read pos and vel of the wheels from the encoder and put them into the joints_ structure
  // and send the new command to the motor
  my_eden.updateHardware();

  controller_manager.update(ros::Time::now(), elapsed, false);

}


int main(int argc, char **argv)
{
  ROS_INFO("Test Moteurs");

  ros::init(argc, argv, "brouette_hardware");

  if(argc != 1)
	ROS_ERROR("** Incorrect input! **");
  else {
    ros::NodeHandle nh;
    ros::CallbackQueue eden_callback_queue;
    nh.setCallbackQueue(&eden_callback_queue);

    double control_frequency = 100.0;
    double diagnostic_frequency = 1.0;
    //nh.getParam("control_frequency", control_frequency);
    //nh.getParam("diagnostic_frequency", diagnostic_frequency);

    Eden my_eden(&nh);
    controller_manager::ControllerManager controller_manager(&my_eden, nh);

    ros::AsyncSpinner spinner(4, &eden_callback_queue);

    time_source::time_point last_time = time_source::now();

    ros::TimerOptions control_timer(
      ros::Duration(1 / control_frequency),
      boost::bind(controlLoop, boost::ref(my_eden), boost::ref(controller_manager), boost::ref(last_time)),
      &eden_callback_queue);
    ros::Timer control_loop = nh.createTimer(control_timer);

    /*ros::TimerOptions diagnostic_timer(
      ros::Duration(1 / diagnostic_frequency),
      boost::bind(diagnosticLoop, boost::ref(md25)),
      &a390_queue);
    ros::Timer diagnostic_loop = nh.createTimer(diagnostic_timer);
    */
    spinner.start();

    // envoyer une commande moteur a 128 (0 rad/s)

    ros::spin();

    spinner.stop();

    // Restore port default before closing
 	  my_eden.restorePort();
    ROS_INFO("USS2I2C - port closed");

  }
  return 0;
}