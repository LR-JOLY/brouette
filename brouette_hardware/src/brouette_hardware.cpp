#include <brouette_hardware/brouette_hardware.hpp>
#include <boost/assign/list_of.hpp>
#include "controller_manager/controller_manager.h"
#include "hardware_interface/actuator_state_interface.h"
#include "math.h"

Brouette::Brouette(ros::NodeHandle* nodehandle):ros_nh_(*nodehandle)
{
  // TODO : take data in a config file
  double PID_steering_motor[3] = {3.61275, 0.61383, 0.0}; // QPPSmax 4500
  double PID_drive_motor[3] = {3.05391, 0.32801, 0.0}; // QPPSmax 7875
  double steering_encoder_incr_number = 121.0;
  double drive_encoder_incr_number = 121.0;
  double steering_transmission_ratio = 810.0;
  double drive_transmission_ratio = 17.0*40.0/100.0;
  addMotor("fr_leg_joint","JointPositionController",MOTORBOARD_ONE, STEERING_MOTOR_POS, steering_encoder_incr_number, steering_transmission_ratio, PID_steering_motor);
  addMotor("fr_wheel_joint","JointVelocityController",MOTORBOARD_ONE, DRIVE_MOTOR_POS, drive_encoder_incr_number, drive_transmission_ratio, PID_drive_motor);
  addMotor("fl_leg_joint","JointPositionController",MOTORBOARD_TWO, STEERING_MOTOR_POS, steering_encoder_incr_number, steering_transmission_ratio, PID_steering_motor);
  addMotor("fl_wheel_joint","JointVelocityController",MOTORBOARD_TWO, DRIVE_MOTOR_POS, drive_encoder_incr_number, drive_transmission_ratio, PID_drive_motor);
  addMotor("rr_leg_joint","JointPositionController",MOTORBOARD_THREE, STEERING_MOTOR_POS, steering_encoder_incr_number, steering_transmission_ratio, PID_steering_motor);
  addMotor("rr_wheel_joint","JointVelocityController",MOTORBOARD_THREE, DRIVE_MOTOR_POS, drive_encoder_incr_number, drive_transmission_ratio, PID_drive_motor);
  addMotor("rl_leg_joint","JointPositionController",MOTORBOARD_FOUR, STEERING_MOTOR_POS, steering_encoder_incr_number, steering_transmission_ratio, PID_steering_motor);
  addMotor("rl_wheel_joint","JointVelocityController",MOTORBOARD_FOUR, DRIVE_MOTOR_POS, drive_encoder_incr_number, drive_transmission_ratio, PID_drive_motor);


  ros::V_string joint_names = getJointNameVector();
  
  for (unsigned int i = 0; i < joint_names.size(); i++)
  {
    hardware_interface::JointStateHandle joint_state_handle(joint_names[i],
                                                            &joints_[i].position, &joints_[i].velocity,
                                                            &joints_[i].effort);
    joint_state_interface_.registerHandle(joint_state_handle);
    hardware_interface::JointHandle joint_handle(joint_state_handle, &joints_[i].command);

    if ( i % 2 == 0)
    {
        position_joint_interface_.registerHandle(joint_handle);
    }
    else
    {
        velocity_joint_interface_.registerHandle(joint_handle);
    }

  }
  registerInterface(&joint_state_interface_);
  registerInterface(&velocity_joint_interface_);
  registerInterface(&position_joint_interface_);
}

Brouette::~Brouette(){
  //DESTROYPTR();
  ROS_WARN("Brouette/Destructor - object killed");
}

void Brouette::addMotor(std::string joint_name, std::string controller_type, int board_index, int position, int encoder_increment_number, double transmission_ratio, double pid[3])
{
    Motor motor(joint_name, controller_type, board_index, position, encoder_increment_number, transmission_ratio, pid);
    motors_list_.push_back(motor);

}

ros::V_string Brouette::getJointNameVector()
{
    ros::V_string joint_name_vector;
    for (Motor motor : motors_list_){
        joint_name_vector.push_back(motor.getJointName());
    }
    return joint_name_vector;
}

void Brouette::restorePort()
{
  roboclaw_network_.closeCommunication();
}

void Brouette::getTarget(void)
{
    for (int i = 0; i<MOTORS_NUMBER; i++) {
        target_[i] = joints_[i].command;
    }
}

void Brouette::setTarget(void)
{
    int i = 0;
    for (auto motor = motors_list_.begin(); motor != motors_list_.end(); ++motor)
    {
        motor->setTarget(target_[i]);
        ++i;
    }
}

void Brouette::updateHardware(void)
{
    int i = 0;
    for (auto motor = motors_list_.begin(); motor != motors_list_.end(); ++motor)
    {
        if (i<2) // We have only one roboclaw for instance
        {
            uint32_t *encoder = new uint32_t;
            uint8_t *status = new uint8_t;
            double *position;
            double *velocity;
            roboclaw_network_.readEncoder(motor->getBoardIndex(), motor->getPositionIndex(),encoder, status);
            double data[2] = {0};
            motor->populatePositionMeasurement(*encoder,data);
            joints_[i].position = data[0]; // WARNING : encoder roll over not covered !!!
            joints_[i].velocity = data[1];
            delete encoder;
            delete status;
        }
        else
        {
            joints_[i].position = 0.0;
            joints_[i].velocity = 0.0;
        }
        ++i;
    }
    setTarget();
    // Send the new command to the motor
    updateMotorSpeed();
}

void Brouette::updateMotorSpeed(void)
{
    int i = 0;
    for (auto motor = motors_list_.begin(); motor != motors_list_.end(); ++motor)
    {
        if (i<2){
            if (motor->getType().compare("JointPositionController")==0)
            {
                // TODO
                //ROS_INFO("code not completed");
            }
            else if (motor->getType().compare("JointVelocityController")==0)
            {
                //ROS_INFO("%f",motor->getCommand());
                roboclaw_network_.driveMotor(motor->getBoardIndex(), motor->getPositionIndex(),motor->getCommand());
            }
        }
        ++i;
    } 
}
