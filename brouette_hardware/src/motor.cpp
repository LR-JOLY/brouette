#include "brouette_hardware/motor.h"

Motor::Motor(std::string joint_name, std::string controller_type, int board_index, int position_index, double encoder_increment_number, double transmission_ratio, double pid[3])
{
    motorboard_index_ = board_index;
    joint_name_ = joint_name;
    controller_type_ = controller_type;
    tick_per_rev_ = (double)encoder_increment_number * transmission_ratio;
    p_ = pid[0];
    i_ = pid[1];
    d_ = pid[2];
    position_index_ = position_index;
    previous_time_ = ros::Time::now();
}


void Motor::setBoardIndex(int index)
{
    motorboard_index_ = index;
}

uint8_t Motor::getBoardIndex()
{
    return motorboard_index_;
}

uint8_t Motor::getPositionIndex(void)
{
    return position_index_;
}

double *Motor::getPID(void)
{
   double *PID= new double[3];
   PID[0] = p_;
   PID[1] = i_;
   PID[2] = d_;
   return PID;
}

std::string Motor::getJointName(void)
{
    return joint_name_;
}

double Motor::getDuration(void)
{
    ros::Time present_time = ros::Time::now();
    ros::Duration duration = present_time - previous_time_;
    this->setPreviousTime(present_time);
    return duration.toSec();
}


void Motor::setPreviousTime(ros::Time time)
{
    previous_time_ =  time;
}


void Motor::setTarget(double target)
{
    target_ = target;
}

void Motor::populatePositionMeasurement(double encoder_value, double data[2])
{
    double position = 2.0 * M_PI * encoder_value / tick_per_rev_;
    double time_difference = getDuration();
    double velocity = (position - previous_position_)/ time_difference;
    command_ = target_;
    previous_position_ = position;
    data[0] = position;
    data[1] = velocity;
}

double Motor::getCommand()
{
    return command_;
}

std::string Motor::getType()
{
    return controller_type_;
}