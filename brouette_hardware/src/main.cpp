#include <brouette_hardware/brouette_hardware.hpp>
#include "controller_manager/controller_manager.h"
#include <ros/callback_queue.h>

#include <boost/chrono.hpp>

typedef boost::chrono::steady_clock time_source;

void controlLoop(Brouette &my_brouette,
                 controller_manager::ControllerManager &controller_manager,
                 time_source::time_point &last_time)
{

  // Calculate monotonic time difference
  time_source::time_point this_time = time_source::now();
  boost::chrono::duration<double> elapsed_duration = this_time - last_time;
  ros::Duration elapsed(elapsed_duration.count());
  last_time = this_time;

  // Process control loop
  // Get velocity target from holomonic controller
  my_brouette.getTarget();
  //ROS_WARN("");
  // Read pos and vel of the wheels from the encoder and put them into the joints_ structure
  // and send the new command to the motor
  my_brouette.updateHardware();
  controller_manager.update(ros::Time::now(), elapsed, false);

}


int main(int argc, char **argv)
{
  ROS_INFO("Test Moteurs");

  ros::init(argc, argv, "brouette_hardware");

  if(argc != 1)
	ROS_ERROR("** Incorrect input! **");
  else {
    ros::NodeHandle nh;
    ros::CallbackQueue brouette_callback_queue;
    nh.setCallbackQueue(&brouette_callback_queue);

    double control_frequency = 100.0;
    double diagnostic_frequency = 1.0;
    //nh.getParam("control_frequency", control_frequency);
    //nh.getParam("diagnostic_frequency", diagnostic_frequency);

    Brouette my_brouette(&nh);
    controller_manager::ControllerManager controller_manager(&my_brouette, nh);

    ros::AsyncSpinner spinner(8, &brouette_callback_queue);

    time_source::time_point last_time = time_source::now();

    ros::TimerOptions control_timer(
      ros::Duration(1 / control_frequency),
      boost::bind(controlLoop, boost::ref(my_brouette), boost::ref(controller_manager), boost::ref(last_time)),
      &brouette_callback_queue);
    ros::Timer control_loop = nh.createTimer(control_timer);

    spinner.start();

    ros::spin();

    spinner.stop();

    // Restore port default before closing
 	  my_brouette.restorePort();
    ROS_INFO("Connexion to roboclaw - port closed");

  }
  return 0;
}