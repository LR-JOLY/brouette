#include "ros/ros.h"
#include <string>

class Motor{
  public:
    Motor(std::string joint_name, std::string controller_type, int index, int position, double encoder_increment_number, double transmission_ratio, double pid[3]);
    void setBoardIndex(int index);
    uint8_t getBoardIndex(void);
    double getDuration(void);
    void setPID(double p, double i, double d);
    double *getPID(void);
    std::string getJointName(void);
    void setTarget(double target);
    void populatePositionMeasurement(double encoder_value, double data[2]);
    double getCommand(void);
    uint8_t getPositionIndex(void);
    std::string getType();
    void setPreviousTime(ros::Time time);

  private:
    uint8_t motorboard_index_;
    uint8_t position_index_;
    std::string joint_name_;
    std::string controller_type_;
    //int encoder_increment_number_;
    //double transmission_ratio_;
    double tick_per_rev_;
    double p_ = 1.0;
    double i_ = 0.0;
    double d_ = 0.0;
    double target_ = 0.0;
    double previous_target_ = 0.0;
    int command_ = 0; // 0 to 255
    double previous_position_ = 0.0;
    ros::Time previous_time_;

};
