#ifndef EDEN_HARDWARE_H
#define EDEN_HARDWARE_H

#include "roboclaw/roboclaw.hpp"
#include "brouette_hardware/motor.h"

#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include "ros/ros.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Int64.h"
#include <realtime_tools/realtime_publisher.h>

#include <string>
#include <iostream>
#include <vector>
#include <ros/callback_queue.h>
#include "controller_manager/controller_manager.h"


#define MOTORBOARD_ONE                0x80
#define MOTORBOARD_TWO                0x81
#define MOTORBOARD_THREE              0x82
#define MOTORBOARD_FOUR               0x83
#define MOTORS_NUMBER                 8
#define STEERING_MOTOR_POS            1
#define DRIVE_MOTOR_POS               2

/**
 * @class       Brouette "brouette_hardware.hpp"
 * @brief       declaration of the Brouette class 
 * @details     The \c Brouette class is the \em ROS \em driver for \b Brouette hardware
 * @author      Louis-Romain JOLY <louis-romain.joly@sncf.fr>
 * @version     0.0
 * @date        2021
 * @note        TBD
 * @pre         TBD
 * @post        TBD
 * @bug         TBD
 * @warning     A bad use may distroy your hardware
 * @attention   TBD
 * @remark      TBD
 * @copyright   GNU Public License.
 */class Brouette : public hardware_interface::RobotHW
{
public:
  Brouette(ros::NodeHandle* nodehandle);
  ~Brouette();

  void getTarget(void);
  void setTarget(void);
  void restorePort(void);
  void updateHardware(void);

private:

  void updateMotorSpeed(void);
  void addMotor(std::string joint_name, std::string controller_type, int board_index, int position, int encoder_increment_number, double transmission_ratio, double pid[3]);
  ros::V_string getJointNameVector();

  // ROS Control interfaces
  hardware_interface::JointStateInterface joint_state_interface_;
  hardware_interface::VelocityJointInterface velocity_joint_interface_;
  hardware_interface::PositionJointInterface position_joint_interface_;

  Roboclaw roboclaw_network_ {"/dev/roboclaw",115200};;
  std::list<Motor> motors_list_;
  double target_[MOTORS_NUMBER] = {};
  double position_[MOTORS_NUMBER] = {};
  double velocity_[MOTORS_NUMBER] = {};
  double effort_[MOTORS_NUMBER] = {};
  std::string leg_name_array_[4] = {"fl","fr","rl","rr"}; // wheel_array
  int encoder_[MOTORS_NUMBER];

  ros::NodeHandle ros_nh_;

  struct Joint
  {
    double position;
    double position_offset;
    double velocity;
    double effort;
    double command; //position or velocity
    double velocity_command;

    Joint() :
      position(0), velocity(0), effort(0), command(0)
    { }
  } joints_[MOTORS_NUMBER];  
};



#endif