#ifndef MOTORBOARDS_SUPERVISER_H_
#define MOTORBOARDS_SUPERVISER_H_

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include "termios.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "sys/types.h"
#include "math.h"
#include "ros/ros.h"
#include "sstream"
#include "std_msgs/Float64.h"
#include "std_msgs/Int64.h"
#include "time.h"
#include "brouette_hardware/motorboard_driver.h"
#include "brouette_hardware/motor.h"


#define BOARDS_NUMBER         4
#define MOTORS_NUMBER         8 // 2 motors per board

class MotorboardSuperviser
{
private:

    ros::Publisher ros_battery_voltage_publisher_;
    ros::Publisher ros_wheel_speed_publisher_;

    void initialiseList(int value, int list[MOTORS_NUMBER]);
    void initialiseList(double value, double list[MOTORS_NUMBER]);

    int getCommunicationDevicePointer(void);

    /* data */
    char *device_;
    int battery_voltage_ = 0;
    int communication_device_pointer_;
    std::list<MotorBoard> motorboard_list;
    std::list<Motor> motor_list;
    ros::Time previous_time_[MOTORS_NUMBER];
    int velocity_command_[MOTORS_NUMBER];
    double velocity_measurement_[MOTORS_NUMBER];



public:
    //MotorboardSuperviser(ros::NodeHandle *nh);
    MotorboardSuperviser();
    ~MotorboardSuperviser();
    int setMotorboardList(unsigned char motorboard_address);
    void openCommunicationDevice(const char *device);
    void wakeUpBoards(void);
    void closeCommunicationDevice(void);
    void readEncoderData(double *pos[MOTORS_NUMBER], double *vel[MOTORS_NUMBER]);
    void addMotor(std::string joint_name, std::string controller_type, unsigned char motorboard_address, int encoder_increment_number, double transmission_ratio, double PID[3]);
    ros::V_string getJointNameVector(void);
    void setTarget(double target[MOTORS_NUMBER]);
    void updateMotorSpeed(void);
};

#endif