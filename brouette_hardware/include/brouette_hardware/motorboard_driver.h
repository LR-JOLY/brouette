#ifndef MOTORBOARD_DRIVER_H_
#define MOTORBOARD_DRIVER_H_

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include "termios.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "sys/types.h"
#include "math.h"
#include "ros/ros.h"
#include "sstream"
#include "std_msgs/Float64.h"
#include "std_msgs/Int64.h"
#include "time.h"

#define ENCODER_A_REGISTER         0x02
#define ENCODER_B_REGISTER         0x06
#define MOTOR_A_REGISTER           0x00
#define MOTOR_B_REGISTER           0x01

class MotorBoard
{
public:
    MotorBoard(const unsigned char address);
    ~MotorBoard();
    
    void changeI2cBusAddress(int file_pointer);	
    void setI2cMode(int file_pointer);		// Set the MD25 into I2C mode, 100KHz clock
    /* Config functions*/
    void disableTimeout(int file_pointer, unsigned char driver_address);
    void resetEncoder(int file_pointer, unsigned char driver_address);
    void disableSpeedReg(int file_pointer, unsigned char driver_address);
    void changeMode(int file_pointer, unsigned char driver_address); // Not implemented in cpp file
    void displayVersion(int file_pointer);
    /* writing functions */
    void setMotorSpeed(int file_pointer, unsigned char driver_address, int motor_register_number, unsigned char speed);
    /* reading functions */
    int readEncoder(int file_pointer, unsigned char driver_address, int encoder_register_number);
    void readBatteryVoltage(int file_pointer, unsigned char driver_address);
    void readMode(int file_pointer, unsigned char driver_address);  // Not implemented in cpp file
    /* access to data functions */
    unsigned char getMotorRegistrer(int index);
    unsigned char getEncoderRegistrer(int index);
    unsigned char getError(void);
    unsigned char getAddress();
    double getBatteryVoltage(void);
    bool getDebugMode(void);
    

private:
    unsigned char address_;
    unsigned char encoder_register_[2];
    unsigned char motor_register_[2];
    unsigned char buffer_[20];			// serial buffer for r/w

    unsigned char error_ = 0x00;		// Byte used to indicate errors

    int battery_voltage_;

    // PID Characteristics
    //double P = 10.0;
    //double I = 0.0;
    //double D = 0.0;
    

    // DEBUG
    bool debug_ = false;

};

#endif