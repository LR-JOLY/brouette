#ifndef EDEN_HARDWARE_H
#define EDEN_HARDWARE_H

#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include "ros/ros.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Int64.h"
#include <realtime_tools/realtime_publisher.h>

#include <string>
#include <iostream>
#include <vector>
#include <ros/callback_queue.h>
#include "controller_manager/controller_manager.h"
#include "brouette_hardware/motorboard_superviser.h"

#define FRONT_RIGHT_MOTORBOARD        0xB0
#define FRONT_LEFT_MOTORBOARD         0xB2
#define REAR_RIGHT_MOTORBOARD         0xB4
#define REAR_LEFT_MOTORBOARD          0xB6

class Eden : public hardware_interface::RobotHW
{
public:
  Eden(ros::NodeHandle* nodehandle);
  ~Eden();

  void getTarget(void);
  void updateHardware(void);
  void restorePort(void);

private:
  // ROS Control interfaces
  hardware_interface::JointStateInterface joint_state_interface_;
  hardware_interface::VelocityJointInterface velocity_joint_interface_;
  hardware_interface::PositionJointInterface position_joint_interface_;

  MotorboardSuperviser usb2iss_;
  double target_[MOTORS_NUMBER] = {};
  double position_[MOTORS_NUMBER] = {};
  double velocity_[MOTORS_NUMBER] = {};
  double effort_[MOTORS_NUMBER] = {};
  std::string leg_name_array_[4] = {"fl","fr","rl","rr"}; // old wheel_array
  int encoder_[MOTORS_NUMBER];

  //double wheel_velocity[MOTORS_NUMBER];

  ros::NodeHandle nh_;

  struct Joint
  {
    double position;
    double position_offset;
    double velocity;
    double effort;
    double command; //position or velocity
    double velocity_command;

    Joint() :
      position(0), velocity(0), effort(0), command(0)
    { }
  } joints_[MOTORS_NUMBER];  
};

#endif
